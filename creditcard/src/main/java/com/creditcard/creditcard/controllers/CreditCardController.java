package com.creditcard.creditcard.controllers;

import javax.validation.Valid;

import com.DTOs.RequestCreditCard;
import com.DTOs.RequestCreditCardActivated;
import com.creditcard.creditcard.client.Client;
import com.creditcard.creditcard.client.CreditcardClient;
import com.creditcard.creditcard.mappers.CreditCardMapper;
import com.creditcard.creditcard.models.CreditCard;
import com.creditcard.creditcard.services.CreditCardService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.http.HttpStatus;

@RestController
@RequestMapping("/creditcard")
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMapper creditCardMapper;

    @Autowired
    CreditcardClient clientClient;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreditCard createCreditCard(@RequestBody @Valid RequestCreditCard requestCreditCard) {

        CreditCard creditCard = creditCardMapper.convertToModel(requestCreditCard);

        Client client = clientClient.getClientById(creditCard.getClientId());

        if (client == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST , "Client not found");
        }

       return creditCardService.saveCreditCard(creditCard);
    }

    @PatchMapping("/{number}")
    public CreditCard updateCreditCard(@PathVariable(name = "number") String number, @RequestBody RequestCreditCardActivated requestCreditCardActivated) {
        return creditCardService.updateCreditCardStatus(number, creditCardMapper.convertToModelActivated(requestCreditCardActivated));
    }

    @GetMapping("/{number}")
    public CreditCard findCreditCard(@PathVariable(name = "number") String number) {
        return creditCardService.getCreditCardByNumber(number);
    }

    @GetMapping()
    public CreditCard findCreditCardById(@RequestParam(name = "id") int id) {
        return creditCardService.getCreditCardById(id);
    }
    
}