package com.creditcard.creditcard.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Client ID not found")
public class ClientNotFoundException extends RuntimeException {
    
}