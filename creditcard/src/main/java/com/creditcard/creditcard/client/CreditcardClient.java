package com.creditcard.creditcard.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "client", configuration = CreditcardClientConfiguration.class)
public interface CreditcardClient {

    @GetMapping("client/{id}")
    Client getClientById(@PathVariable int id);
    
}