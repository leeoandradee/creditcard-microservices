package com.creditcard.creditcard.client;

import org.springframework.context.annotation.Bean;

import feign.codec.ErrorDecoder;

public class CreditcardClientConfiguration {

    @Bean
    public ErrorDecoder getCreditcardClientDecoder() {
        return new CreditcardClientDecoder();
    }
    
}