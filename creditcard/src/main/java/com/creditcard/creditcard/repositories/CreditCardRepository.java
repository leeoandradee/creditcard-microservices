package com.creditcard.creditcard.repositories;

import java.util.Optional;

import com.creditcard.creditcard.models.CreditCard;

import org.springframework.data.repository.CrudRepository;

public interface CreditCardRepository extends CrudRepository<CreditCard, Integer> {


    Optional<CreditCard> findByNumber(String number);
    
}