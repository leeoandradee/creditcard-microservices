package com.creditcard.creditcard.services;

import java.util.Optional;

import com.creditcard.creditcard.client.Client;
import com.creditcard.creditcard.client.CreditcardClient;
import com.creditcard.creditcard.models.CreditCard;
import com.creditcard.creditcard.repositories.CreditCardRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CreditCardService {

    @Autowired
    CreditCardRepository creditCardRepository;

    public CreditCard saveCreditCard(CreditCard creditCard) {
        return creditCardRepository.save(creditCard);
    }

    public CreditCard updateCreditCardStatus(String number, CreditCard creditCard) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findByNumber(number);
        if (creditCardOptional.isPresent()) {
            creditCard.setId(creditCardOptional.get().getId());
            creditCard.setNumber(creditCardOptional.get().getNumber());
            creditCard.setClientId(creditCardOptional.get().getClientId());
            creditCard.setActivated(creditCardOptional.get().isActivated());

            return creditCardRepository.save(creditCard);
        } else {
            throw new RuntimeException("Card not found");
        }
    }

    public CreditCard getCreditCardByNumber(String number) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findByNumber(number);
        if (creditCardOptional.isPresent()) {
            return creditCardOptional.get();
        } else {
            throw new RuntimeException("Card not found");
        }
    }

    public CreditCard getCreditCardById(int id) {
        Optional<CreditCard> creditCardOptional = creditCardRepository.findById(id);
        if (creditCardOptional.isPresent()) {
            return creditCardOptional.get();
        } else {
            throw new RuntimeException("Card not found");
        }
    }


    
}