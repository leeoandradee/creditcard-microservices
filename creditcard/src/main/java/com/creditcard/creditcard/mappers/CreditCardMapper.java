package com.creditcard.creditcard.mappers;

import com.DTOs.RequestCreditCard;
import com.DTOs.RequestCreditCardActivated;
import com.creditcard.creditcard.models.CreditCard;

import org.springframework.stereotype.Component;

@Component
public class CreditCardMapper {

    public CreditCard convertToModel(RequestCreditCard requestCreditCard) {

        CreditCard creditCard = new CreditCard();
        creditCard.setNumber(requestCreditCard.getNumber());
        creditCard.setClientId(requestCreditCard.getClientId());

        return creditCard;
    }

    public CreditCard convertToModelActivated(RequestCreditCardActivated requestCreditCardActivated) {

        CreditCard creditCard = new CreditCard();
        creditCard.setActivated(requestCreditCardActivated.isActivated());

        return creditCard;
    }
    
}