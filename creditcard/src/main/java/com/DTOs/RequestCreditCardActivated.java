package com.DTOs;

public class RequestCreditCardActivated {

    private boolean activated;

    public RequestCreditCardActivated() {
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}