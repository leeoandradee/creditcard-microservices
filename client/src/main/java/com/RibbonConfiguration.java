package com;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;

import org.springframework.context.annotation.Bean;

public class RibbonConfiguration {

    //Configurar a ordem de chamadas das inatacias dos microserviços
    @Bean
    public IRule getRule() {
        return new RandomRule();
    }

}