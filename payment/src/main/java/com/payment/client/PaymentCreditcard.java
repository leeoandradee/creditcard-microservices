package com.payment.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "creditcard", configuration = PaymentCreditcardConfiguration.class)
public interface PaymentCreditcard {

    @GetMapping("/creditcard/{creditCardId}")
    CreditCard getByCreditCardId(@PathVariable int creditCardId);
}