package com.payment.client;

import feign.Response;
import feign.codec.ErrorDecoder;

public class PaymentCreditcardDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status()  == 404) {
            return new CreditcardNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
    
}