package com.payment.client;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Credit card not found")
public class CreditcardNotFoundException extends RuntimeException {
    
}