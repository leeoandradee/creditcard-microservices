package com.payment.repositories;

import com.payment.models.Payment;

import org.springframework.data.repository.CrudRepository;

public interface PaymentRepository extends CrudRepository<Payment, Integer>{

    Iterable<Payment> findByCreditCardId(int id);
    
}