package com.payment.controllers;

import java.util.List;

import com.payment.client.CreditCard;
import com.payment.client.PaymentCreditcard;
import com.payment.models.Payment;
import com.payment.services.PaymentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @Autowired
    private PaymentCreditcard paymentCreditcard;

    @PostMapping()
    public Payment createPayment(@RequestBody Payment payment) {

        CreditCard creditCard = paymentCreditcard.getByCreditCardId(payment.getCreditCardId());

        if (creditCard.isActivated()) {
            System.out.println("Creating payment " + payment.getCreditCardId() + " at " + System.currentTimeMillis());
            return paymentService.savePayment(payment);
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Credit card not activated");
        }
    }

    @GetMapping("/{creditCardId}")
    public List<Payment> listPaymentsByCreditCardId(@PathVariable(name = "creditCardId") int creditCardId) {
        return paymentService.getPaymentsByCreditCardNumber(creditCardId);
    }
    
}