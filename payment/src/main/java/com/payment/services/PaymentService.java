package com.payment.services;

import java.util.List;

import com.payment.models.Payment;
import com.payment.repositories.PaymentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {

    @Autowired
    PaymentRepository paymentRepository;

    public Payment savePayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public List<Payment> getPaymentsByCreditCardNumber(int creditCardId) {
        return (List<Payment>) paymentRepository.findByCreditCardId(creditCardId);
    }
    
}